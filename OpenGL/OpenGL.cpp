// OpenGL.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <math.h>
#include <vector>

struct Point {
	int X;
	int Y;

	Point(int x, int y) {
		X = x;
		Y = y;
	}
};

void init2D(float r, float g, float b) {
	glClearColor(r, g, b, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0, 400.0, 0, 400.0);
}

std::vector<Point> getCentralCirclePoints(int radius) {
	std::vector<Point> result;
	int y = radius;
	int x = 0;
	int squareRadius = pow(radius, 2);
	result.push_back(Point(x, y));
	result.push_back(Point(-x, y));
	result.push_back(Point(x, -y));
	result.push_back(Point(-x, -y));
	double d = 0;
	while(x != radius || y != 0) {
		double s = (pow(x + 1, 2) + pow(y, 2)) - squareRadius;
		double t = (pow(x, 2) + pow(y - 1, 2)) - squareRadius;
		d = abs(s) - abs(t);
		if (d >= 0) {
			y--;
		}
		else {
			x++;
		}
		result.push_back(Point(x, y));
		result.push_back(Point(-x, y));
		result.push_back(Point(x, -y));
		result.push_back(Point(-x, -y));
	}
	return result;
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_POINTS);
	int xOffset = 200;
	int yOffset = 200;
	auto points = getCentralCirclePoints(100);
	for each (auto point in points) {
		glVertex2i(point.X + xOffset, point.Y + yOffset);
	}
	glEnd();
	glFlush();
}

int getDelta(int current, int next) {
	return next - current;
}


int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("points and lines");
	init2D(0.0, 0.0, 0.0);
	glutDisplayFunc(display);
	glutMainLoop();
}

